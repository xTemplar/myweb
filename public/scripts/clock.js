 
var ClockMech = React.createClass({


    render: function() {
        var today = this.props.date;
        var secNow= today.getSeconds();
        var minNow=today.getMinutes();
        var hourNow= today.getHours();

        // transform time to 360 deg 
        var millis = today.getMilliseconds();
        var second = (secNow  + (millis / 1000))* 6;
        var minute = (minNow  + (secNow / 60 ))* 6;
        var hour = (hourNow + (minNow / 60))* 15;
      
        // Transform time to hex color
        var colorSec =Math.floor(secNow * 4.2 + millis * (4.2 / 1000) ); // change 60s and ms to FF
            colorSec = (colorSec < 16 ? '0' : '') + colorSec.toString(16).toUpperCase(); // to avoid one number from 0-F

        var colorMin =Math.floor(minNow * 4.2 + secNow*(4.2/ 60) );
            colorMin = (colorMin < 16 ? '0' : '') + colorMin.toString(16).toUpperCase();

        var colorHour = Math.floor(hourNow*10 + (minNow/ 6) );
            colorHour = (colorHour < 16 ? '0' : '') + colorHour.toString(16).toUpperCase();
        
        var fullColor= "#"+colorHour + colorMin + colorSec;    
        var colorM="#00" + colorMin + "00";   
        var colorH="#" + colorHour + "0000";  

        return(
            <div>
                <img src={"clockBack.jpg"} alt="clockWall" />
                <h1> Color-Clock </h1>
                <div>
                           
                    <div className="clockRing">
                        
                        <div className="ringBack" style={toggleVis(hour, colorH)} ></div>
                        <div className="ring" style={transformDeg(hour, colorH)}></div>
                        <div className="ringFront" style={toggleVis(hour,"#606060")} ></div>
                        <p>{addZero(hourNow)}</p>
                    </div>
                    <div className="clockRing">
                         
                        <div className="ringBack" style={toggleVis(minute,colorM)} ></div>
                        <div className="ring" style={transformDeg(minute, colorM)}></div>
                        <div className="ringFront" style={toggleVis(minute,"#606060")} ></div>
                        <p>{addZero(minNow)}</p>

                    </div>
                    <div className="clockRing">
                         
                        <div className="ringBack" style={toggleVis(second,fullColor)} ></div>
                        <div className="ring" style={transformDeg(second, fullColor)}></div>
                        <div className="ringFront" style={toggleVis(second,"#606060")} ></div>
                        <p>{addZero(secNow)}</p>
                    </div>

                </div>
                <div className="colorTable">
                    <p>Color</p> 
                    <p style={changeColor(fullColor)} >{fullColor} </p>    
                    <p> RGB: </p> 
                    <div className="gradLine"  style={changeCSSgradient(52,"#300000","#ff0000")} >  </div> 
                    <div className="colorBox" style={ moveBoxGradient( 50, hour)} >  {colorHour} </div> 

                    <div className="gradLine"  style={changeCSSgradient(67,"#003000","#00ff00")} >  </div> 
                    <div className="colorBox" style={ moveBoxGradient( 65, minute)} >  {colorMin} </div> 

                    <div className="gradLine"  style={changeCSSgradient(82,"#000030","#0000ff")} >  </div> 
                    <div className="colorBox" style={ moveBoxGradient( 80, second)} >  {colorSec} </div> 
                </div>
                  
             </div>
            
            );
    }
});

function addZero(time){ //add zero if h/min/s <10
return  (time < 10 ? '0' : '') + time;
}

function transformDeg(deg, col){
    
    return {  
        transform: 'rotate(' + (deg + 45) + 'deg)',
        borderBottomColor: col, 
        borderLeftColor: col
    };
}

function toggleVis(deg,changeColor){
    var vis=0; 
     
    if( (changeColor =='#606060') && deg <181){ //left half-ring, visible only 0-180
        vis=1;
    }
    else if( (changeColor !='#606060') && deg >180 ){ // right half-ring, visible only 181-360
        vis=1;
    }

    return { 
        opacity: vis,
        borderBottomColor: changeColor,
        borderLeftColor: changeColor
     };
} 

function moveBoxGradient( marginT, marginL){
    return { 
        marginTop: marginT,
        marginLeft: marginL/3 };
}

function changeCSSgradient(marg, col1,col2  ){  
    return { 
        background: "linear-gradient(to right, "+col1 +","+ col2+")",
        marginTop:  marg
     };

}

function changeColor(col){
    return{ color: col };
}

var MyClock = React.createClass({
  getInitialState: function() {
    return { date: new Date() };
  },
  componentDidMount: function() {
    this.start();
  },
  start: function() {
    var self = this;
    (function tick() {
      self.setState({ date: new Date() });
      requestAnimationFrame(tick);
    }());
  },
  render: function() {
    return(
        <ClockMech date={this.state.date} />
        );  
  }
});
 

 //ReactDOM.render( <MyClock/>, document.getElementById('clockHolder') );