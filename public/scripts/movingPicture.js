 

function transformDeg( sec, degControl ){
    
    //full cycle =degControl *4

    if( sec > degControl* 3 ){//right to 0 deg
         sec= sec -(degControl*4);    
    }
    else if( sec > degControl ){//move back left
        sec=degControl*2- sec;
    }
    // from 0-50 skip  ( START)
    
    return {  
        transform: 'rotate(' + (sec/4)  + 'deg)',
    };
}

function transformSize(sec, degControl){

    var sizeControl =280; //more  == less change

    if( sec > degControl*3 ){// 150 -200 right to 0 deg
         sec=1 - ((degControl*4 - sec)/sizeControl);    
    }
    else if( sec > degControl*2 ){ //100-150 decrease
        sec=1 + ((degControl*2 - sec)/sizeControl);
    }
    else if( sec > degControl ){ //50-100 move back to normal
        sec=1 - (((degControl*2 )- sec)/sizeControl);
    }
    else{ // from 0-50 decrease size
        sec= 1 - (sec/sizeControl );
    }
     
    return {  
        transform: 'scale(' + sec + ')',   
    };
}
 
var TopPanel= React.createClass({

    getInitialState: function(){// control is for angle ajustment
        return { counter: 0,
                 startPoint: this.props.start, 
                 control: 25
                 };
    },

    componentDidMount: function(){
        
        this.timer = setInterval(this.tick, 50);
    },

    componentWillUnmount: function(){

        clearInterval(this.timer);
    },

    tick: function(){

        
        this.setState({counter: new Date() - this.state.startPoint});
        
        if( this.state.counter >  (this.state.control*4000) ){ // 1s == 1000 in counter
             
           this.setState({startPoint: Date.now() });  
          
        }

    },

    render: function() {
        
       var seconds = Math.round(this.state.counter / 100);
           seconds = (seconds /10).toFixed(1);  
           // seconds=0;  
    //<img src="../img/myPhoto.jpg" />
        return(
          <div>
                <div className="picFilter"></div>
                <div id="photoHolder">
                    <div className="photoFrame" style={transformDeg(seconds, this.state.control)} > </div>
                    <div className="photo" style={transformSize(seconds, this.state.control)}></div>
                    
                </div>    

            </div>

            );
    }
});

ReactDOM.render( <TopPanel start={Date.now()} />, document.getElementById('panelHolder') );
