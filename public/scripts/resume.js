var { Router, Route, IndexRoute, Link, browserHistory } = ReactRouter
 
 var MyMenu = React.createClass({

    componentDidMount: function(){
    //console.log("loc " +document.location);

    },

    getInitialState: function(){
       
        return { focused: 10 };
    },

    clicked: function(index){
        var location;

        switch (index){
        
        case 1:
        location = "/experience";
        break;

        case 2:
        location = "/skills";
        break;

        case 3:
        location = "/projects";
        break;

        case 4:
        location = "/contact";
        break;

        default:
        location = "/";

        }
        this.setState({focused: index});

        browserHistory.push(location);
    },

    render: function() {

        var self = this;

        return (
            <div id="mainMenu" className="menu">
                <ul>{ this.props.items.map(function(m, index){
        
                    var style = '';
        
                    if(self.state.focused == index){
                        style = 'focused';
                    }
        
                    return <li className={style} onClick={ self.clicked.bind(self, index) } >{m}</li>;
                }) }
                        
                </ul>
            </div>
        );

    }
});

 var SubMenu = React.createClass({

    getInitialState: function(){
        return { focused: 10 };
    },

    clicked: function(index){
        var location;

        switch (index){
        
        case 0:
        location = "/projects/1";
        break;

        case 1:
        location = "/projects/2";
        break;

        case 2:
        location = "/projects/3";
        break;

        default:
        location = "/projects";

        }
        this.setState({focused: index});

        browserHistory.push(location);
     
    },

    render: function() {

        var self = this;

        return (
            <div className="menu">
                <ul>{ this.props.items.map(function(m, index){
        
                    var style = '';
        
                    if(self.state.focused == index){
                        style = 'focused';
                    }
        
                    return <li className={style} onClick={ self.clicked.bind(self, index) } >{m}</li>;
        
                }) }
                        
                </ul>
            </div>
        );

    }
});

 var MainLayout = React.createClass({
   
  render: function() {
      var self = this;
    return (
      <div className="app">
      
        <MyMenu items={ ['About me', 'Experience', 'Skills' ,'Projects' , 'Contact'] } />
        <main>
          {this.props.children}
        </main>
      </div>
      )
  }
});

var SearchLayout = React.createClass({
  render: function() {
    return (
      <div className="search">
        <header className="search-header"></header>
        <div className="results">
          {this.props.children}
        </div>
        <div className="search-footer pagination"></div>
      </div>
      )
  }
});
var About = React.createClass({
  render: function() {
    return (
      <div>
        <h2>About me</h2> 
        My name is Adrian Gorny and I have just moved to Poland from Scotland.<br/>
        For the last four years I worked as a Manufacturing Engineer in the electronics industry.<br/>
        Meanwhile, I was working on self-design, programming projects as I'm eager to develop as a Software Developer.<br/>
        I have exceptional analytical skills and enjoy programming in JavaScript, CSS(3) and HTML(5).<br/>
        I appreciate working with others on challenging projects to achieve common goals. <br/>
        I'm always looking forward to new challenges. 
      </div>
    )
  }
});

var History = React.createClass({
  render: function() {
    return (
      <div>
       <h2>Employment history</h2> 
        <h4>Manufacturing Engineer, Plexus Corp. UK (2014.06 - 2016.07)</h4>
          <ul>
            <li>Provide engineering excellence for existing projects and new product introductions. </li>
            <li>Develop and maintain JavaScript on-demand label application for production line.</li>
            <li>Provide various VBA macro tools for engineering team.</li>
            <li>Working to tight deadlines to achieve high customer satisfaction.</li>
          </ul>

        <h4>Manufacturing Engineer, Flextronics International, Poland (2012.11- 2014.05)</h4>
          <ul>
            <li>Responsible for assembly lines and all technical aspects in manufacturing.</li>
            <li>Work with vendors and create relationships with international customers to meet company goals. </li>
            <li>Improve output and product quality by applying continuous improvement and lean principles to all assigned areas. </li>
            <li>Design new tools and jigs using 3D CAD software –SolidWorks.</li>
          </ul>

          <h2>Education</h2> 
            Gdansk University of Technology. Faculty of Electronics, Telecommunications and Informatics - ETI (2006 – 2011).<br/>
            Degree: Master of Science. Specialization: Automatic control and Robotics.<br/>
            Master's thesis: “Controlling robotic teams” - Swarm intelligence, C++ project.
      </div>
      )
  }
});

var Skills = React.createClass({
  render: function() {
    return (
       <div id="skills">
       <h2>Areas of expertise</h2>
       <img src="../img/skills.gif"/> 
       <div>
          <p>JavaScript(ES6)</p>
          <p>HTML(5)</p>
          <p>CSS(3)</p>
          <p>VBA, C++, Node.js</p> 
          <p>React, jQuery</p> 
          <p>Photoshop, GIT</p> 
      </div>  
      </div>
      )
  }
});
 
var Projects = React.createClass({
  render: function() {
      var self = this;
    return (
      <div >
        <SubMenu items={ ['2048 Game', 'Git repository','Label application'] } />
        <div id="subContent" >
          {this.props.children}
        </div>
      </div>
      )
  }
});

var Project1 = React.createClass({
  render: function() {
    return (
    <div className="proj">   
        
        Game is based on "2048" puzzle algorithm <br/>
        It's a self-design JavaScript project <br/>
        Has been developed to support also mobile motion sensor <br/>
        
      <div className="view effect" >  
        <a href="http://xtemplar.bitbucket.org" target="_blank" >
            <img src="../img/game.jpg" />  
            <div className="mask" > </div>
         
            <div className="conInfo" >
              <div className="info" >
                Play "2048"
              </div>
            </div>
        </a> 
      </div> 
    </div>   
      )
  }
});


var Project2 = React.createClass({
  render: function() {
    return (
     <div className="view effect" >  
        <a href="https://bitbucket.org/xTemplar/" target="_blank" >
            <img style={{ left: '75px' }}src="../img/bitbucket.png" />  
            <div className="mask" > </div>
         
            <div className="conInfo" >
              <div className="info" >
                My Bitbucket
              </div>
            </div>
        </a> 
    </div> 
      )
  }
});

var Project3 = React.createClass({
  render: function() {
    return (
       <div className="proj">
          <div >
          Application for producing serial number labels for customer returns or special request products. <br/>
          Reliable and user friendly application which is in use in the factory for a year without any error. <br/>
          Customer complains reduced approximately by 95% <br/>
          </div>
    <img className="projImg" src="../img/label.jpg"/> 
      </div>
      )
  }
});


var Contact = React.createClass({
  

  render: function() {
    return (
       <div>
    <h2>Contact</h2>
     Mobile: 735 616 061 <br/>
     E-Mail: Gorny.adriann@gmail.com<br/>
     Skype: gorny.adriann
      
      </div>
      )
  }
});

MainLayout.contextTypes = {
  router: React.PropTypes.func.isRequired
};

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={MainLayout}>
      <IndexRoute component={About} />
        <Route component={SearchLayout}>
       
        <Route path="experience" component={History} />
        <Route path="skills" component={Skills} />
        <Route path="projects" component={Projects} >
          <Route path="1" component={Project1} />
          <Route path="2" component={Project2} />
          <Route path="3" component={Project3} />
        </Route> 
        <Route path="contact" component={Contact} />
      </Route> 
    </Route>
  </Router>
),  document.getElementById('resumeHolder'));

